using Microsoft.AspNetCore.Mvc;
using SampleMvcApp.Controllers;
using SampleMvcApp.Models;
using System.Collections.Generic; // This is the important directive
using System.Linq;
using Xunit;
namespace SampleMVCApp.Tests
{
    public class ItemsControllerTests
    {
        
        [Fact]
         
            public void Index_ReturnsAViewResult_WithAListOfItems()
            {
                // Arrange
                var controller = new ItemsController();

                // Act
                var result = controller.Index();

                // Assert
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<IEnumerable<Item>>(viewResult.ViewData.Model); // Ensure correct type spelling
                Assert.Equal(3, model.Count());
            }

        }
    }
