﻿using Microsoft.AspNetCore.Mvc;
using SampleMvcApp.Models;
using System.Collections.Generic;

namespace SampleMvcApp.Controllers
{
    public class ItemsController : Controller
    {
        public IActionResult Index()
        {
            var items = new List<Item>
            {
                new Item { Id = 1, Name = "Item 1", Price = 10.0m },
                new Item { Id = 2, Name = "Item 2", Price = 20.0m },
                new Item { Id = 3, Name = "Item 3", Price = 30.0m }
            };
            return View(items); 
            //testing
        }
    }
}
