FROM mcr.microsoft.com/dotnet/aspnet:7.0
 
WORKDIR /app
 
COPY artifacts/ .
 
EXPOSE 80
 
ENTRYPOINT ["dotnet", "SampleMvcApp.dll"]
